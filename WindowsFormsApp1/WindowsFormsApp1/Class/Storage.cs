﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Storage
    {
        private string plainText = null; //Отркытый текст
        private string cipherText = null; //Зашифрованный текст
        private string decryptedText = null; // Дешифрованный текст

        public string PlainText
        {
            get { return plainText; }
            set { plainText = value; }
        }

        public string CipherText
        {
            get { return cipherText; }
            set { cipherText = value; }
        }

        public string DecryptedText
        {
            get { return decryptedText; }
            set { decryptedText = value; }
        }
    }
}
