﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Decrypt : Form
    {
        Storage storage;
        OpenFileDialog openFileDialog = new OpenFileDialog();

        public Decrypt(Storage storage)
        {
            InitializeComponent();
            this.storage = storage;
            if (storage.CipherText != null & storage.CipherText != string.Empty)
                textBox1.Text = storage.CipherText.Replace("|", "");
            if (storage.PlainText != null & storage.PlainText != string.Empty)
                textBox2.Text = storage.PlainText;
        }

        public void OpWind()
        {
            if (storage.CipherText != null & storage.CipherText != string.Empty)
                textBox1.Text = storage.CipherText.Replace("|", "");
            if (storage.PlainText != null & storage.PlainText != string.Empty)
                textBox2.Text = storage.PlainText;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            string ABC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";



            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // читаем файл в строку
            string result = textBox1.Text = File.ReadAllText(openFileDialog.FileName);
            textBox1.Text.Replace("|", "");
            if (openFileDialog.ShowDialog() == DialogResult.Cancel) 
                return;
            // читаем файл в строку
            string[] key = File.ReadAllText(openFileDialog.FileName).Split(new char[] { ';' });

            for (int i = 0; i < ABC.Length; i++)
                result = result.Replace($"{key[i]}", $"{ABC[i]}");

            textBox2.Text = result.Replace("|", "");

            storage.CipherText = textBox1.Text;
            storage.PlainText = textBox2.Text;
        }
    }
}
