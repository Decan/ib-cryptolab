﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Home : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        Storage storage = new Storage();

        public static GraphicsPath RoundedRect(Rectangle baseRect, int radius)
        {
            var diameter = radius * 2;
            var sz = new Size(diameter, diameter);
            var arc = new Rectangle(baseRect.Location, sz);
            var path = new GraphicsPath();

            // Верхний левый угол
            path.AddArc(arc, 180, 90);

            // Верхний правый угол
            arc.X = baseRect.Right - diameter;
            path.AddArc(arc, 270, 90);

            // Нижний правый угол
            arc.Y = baseRect.Bottom - diameter;
            path.AddArc(arc, 0, 90);

            // Нижний левый угол
            arc.X = baseRect.Left;
            path.AddArc(arc, 90, 90);

            path.CloseFigure();
            return path;
        }
        public Home()
        {
            InitializeComponent();
            //customizeDesign();
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        
        private  void customizeDesign()
        {
            panelFileSubMenu.Visible = false;
            panelKeySubMenu.Visible = false;
            panelCipherSubMenu.Visible = false;
        }

        private void hideSubMenu()
        {
            if(panelCipherSubMenu.Visible)
                panelCipherSubMenu.Visible = false;
            if(panelFileSubMenu.Visible)
                panelFileSubMenu.Visible = false;
            if(panelKeySubMenu.Visible)
                panelKeySubMenu.Visible = false;
        }

        private void showSubMenu(Panel subMenu)
        {
            if (!subMenu.Visible)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Region = new Region(
                RoundedRect(
                    new Rectangle(0, 0, this.Width, this.Height)
                    , 10
                )
            );
        }
        #region FileMenu
        private void btnFile_Click(object sender, EventArgs e)
        {
            //showSubMenu(panelFileSubMenu);
        }
        
        private void btnFileOpen_Click(object sender, EventArgs e)
        {
            //
            //Code
            //
            //hideSubMenu();
        }

        private void btnFileSave_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog.FileName;
            // сохраняем текст в файл
            File.WriteAllText(filename, storage.CipherText);
            //
            //Code
            //
            //hideSubMenu();
        }

        private void btnFileSaveAs_Click(object sender, EventArgs e)
        {
            //
            //Code
            //
            //hideSubMenu();
        }
        #endregion
        #region KeyMenu
        private void btnKeyMenu_Click(object sender, EventArgs e)
        {
            //showSubMenu(panelKeySubMenu);
        }

        SaveFileDialog saveFileDialog = new SaveFileDialog();

        private void btnKeyGenerate_Click(object sender, EventArgs e)
        {
            string ABC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            Random random = new Random();
            int[] NumeberABC = new int[33];
            for (int i = 0; i < NumeberABC.Length; i++)
            {
                int temp = random.Next(10, 99);
                while (NumeberABC.Contains(temp))
                    temp = random.Next(10, 99);
                NumeberABC[i] = temp;
            }

            saveFileDialog.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog.FileName;
            // сохраняем текст в файл
            File.WriteAllText(filename, string.Join(";", NumeberABC));
            //
            //Code
            //
            //hideSubMenu();
        }

        private void btnKeyImport_Click(object sender, EventArgs e)
        {
            //
            //Code
            //
            //hideSubMenu();
        }

        private void btnKeyExport_Click(object sender, EventArgs e)
        {
            //
            //Code
            //
            //hideSubMenu();
        }
        #endregion
        #region CipherMenu
        private void btnCipherMenu_Click(object sender, EventArgs e)
        {
            //showSubMenu(panelCipherSubMenu);
        }

        private void btnCipherEncrypt_Click(object sender, EventArgs e)
        {
            Encrypt encrypt = new Encrypt(storage);
            encrypt.OpWind();
            openChildForm(encrypt);
            //
            //Code
            //
            //hideSubMenu();
        }

        private void btnCipherFA_Click(object sender, EventArgs e)
        {
            FA fa = new FA();
            openChildForm(fa);
            //
            //Code
            //
            //hideSubMenu();
        }
        #endregion

        private Form activeForm = null;
        private void openChildForm(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            openChildForm(new Info());
        }
        Storage storage1 = new Storage(); 
        private void btnCipherDecrypt_Click(object sender, EventArgs e)
        {
            Decrypt decrypt = new Decrypt(storage1);
            decrypt.OpWind();
            openChildForm(decrypt);
        }
    }
}
