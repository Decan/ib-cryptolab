﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Encrypt : Form
    {
        Storage storage;
        SaveFileDialog saveFileDialog = new SaveFileDialog();

        public Encrypt(Storage storage)
        {
            InitializeComponent();
            this.storage = storage;
            if (storage.PlainText != null & storage.PlainText != string.Empty)
                textBox1.Text = storage.PlainText;
            if (storage.CipherText != null & storage.CipherText != string.Empty)
                textBox2.Text = storage.CipherText.Replace("|", "");

        }

        public void OpWind()
        {
            if (storage.PlainText != null & storage.PlainText != string.Empty)
                textBox1.Text = storage.PlainText;
            if (storage.CipherText != null & storage.CipherText != string.Empty)
                textBox2.Text = storage.CipherText.Replace("|", "");
        }

        OpenFileDialog openFileDialog = new OpenFileDialog();
        private void button1_Click(object sender, EventArgs e)
        {
            string ABC = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // читаем файл в строку
            textBox1.Text = File.ReadAllText(openFileDialog.FileName);

            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            // читаем файл в строку
            string[] key = File.ReadAllText(openFileDialog.FileName).Split(new char[] { ';' });
            string result = "";
            string str = textBox1.Text.ToUpper();

            for (int i = 0; i < str.Length; i++)
            {
                if (ABC.Contains(str[i]))
                    result += $"{key[ABC.IndexOf(str[i])]}|";
                else result += $"{str[i]}|";
            }

            textBox2.Text = result.Replace("|","");
            storage.PlainText = textBox1.Text;
            storage.CipherText = result;
        }
    }
}
