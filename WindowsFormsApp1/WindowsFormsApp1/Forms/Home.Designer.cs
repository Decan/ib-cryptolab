﻿namespace WindowsFormsApp1
{
    partial class Home
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.btnInfo = new System.Windows.Forms.Button();
            this.panelCipherSubMenu = new System.Windows.Forms.Panel();
            this.btnCipherDecrypt = new System.Windows.Forms.Button();
            this.btnCipherFA = new System.Windows.Forms.Button();
            this.btnCipherEncrypt = new System.Windows.Forms.Button();
            this.btnCipherMenu = new System.Windows.Forms.Button();
            this.panelKeySubMenu = new System.Windows.Forms.Panel();
            this.btnKeyExport = new System.Windows.Forms.Button();
            this.btnKeyImport = new System.Windows.Forms.Button();
            this.btnKeyGenerate = new System.Windows.Forms.Button();
            this.btnKeyMenu = new System.Windows.Forms.Button();
            this.panelFileSubMenu = new System.Windows.Forms.Panel();
            this.btnFileSaveAs = new System.Windows.Forms.Button();
            this.btnFileSave = new System.Windows.Forms.Button();
            this.btnFileOpen = new System.Windows.Forms.Button();
            this.btnFile = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.panelSideMenu.SuspendLayout();
            this.panelCipherSubMenu.SuspendLayout();
            this.panelKeySubMenu.SuspendLayout();
            this.panelFileSubMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelSideMenu.Controls.Add(this.btnInfo);
            this.panelSideMenu.Controls.Add(this.panelCipherSubMenu);
            this.panelSideMenu.Controls.Add(this.btnCipherMenu);
            this.panelSideMenu.Controls.Add(this.panelKeySubMenu);
            this.panelSideMenu.Controls.Add(this.btnKeyMenu);
            this.panelSideMenu.Controls.Add(this.panelFileSubMenu);
            this.panelSideMenu.Controls.Add(this.btnFile);
            this.panelSideMenu.Controls.Add(this.panel1);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(250, 694);
            this.panelSideMenu.TabIndex = 0;
            // 
            // btnInfo
            // 
            this.btnInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInfo.FlatAppearance.BorderSize = 0;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnInfo.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnInfo.Image = global::WindowsFormsApp1.Properties.Resources.outline_info_white_18dp;
            this.btnInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInfo.Location = new System.Drawing.Point(0, 582);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(250, 45);
            this.btnInfo.TabIndex = 8;
            this.btnInfo.Text = "Info";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // panelCipherSubMenu
            // 
            this.panelCipherSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(29)))));
            this.panelCipherSubMenu.Controls.Add(this.btnCipherDecrypt);
            this.panelCipherSubMenu.Controls.Add(this.btnCipherFA);
            this.panelCipherSubMenu.Controls.Add(this.btnCipherEncrypt);
            this.panelCipherSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCipherSubMenu.Location = new System.Drawing.Point(0, 458);
            this.panelCipherSubMenu.Name = "panelCipherSubMenu";
            this.panelCipherSubMenu.Size = new System.Drawing.Size(250, 124);
            this.panelCipherSubMenu.TabIndex = 7;
            // 
            // btnCipherDecrypt
            // 
            this.btnCipherDecrypt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCipherDecrypt.FlatAppearance.BorderSize = 0;
            this.btnCipherDecrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCipherDecrypt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCipherDecrypt.ForeColor = System.Drawing.Color.LightGray;
            this.btnCipherDecrypt.Location = new System.Drawing.Point(0, 80);
            this.btnCipherDecrypt.Name = "btnCipherDecrypt";
            this.btnCipherDecrypt.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnCipherDecrypt.Size = new System.Drawing.Size(250, 40);
            this.btnCipherDecrypt.TabIndex = 2;
            this.btnCipherDecrypt.Text = "Decrypt";
            this.btnCipherDecrypt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCipherDecrypt.UseVisualStyleBackColor = true;
            this.btnCipherDecrypt.Click += new System.EventHandler(this.btnCipherDecrypt_Click);
            // 
            // btnCipherFA
            // 
            this.btnCipherFA.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCipherFA.FlatAppearance.BorderSize = 0;
            this.btnCipherFA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCipherFA.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCipherFA.ForeColor = System.Drawing.Color.LightGray;
            this.btnCipherFA.Location = new System.Drawing.Point(0, 40);
            this.btnCipherFA.Name = "btnCipherFA";
            this.btnCipherFA.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnCipherFA.Size = new System.Drawing.Size(250, 40);
            this.btnCipherFA.TabIndex = 1;
            this.btnCipherFA.Text = "Frequency analysis";
            this.btnCipherFA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCipherFA.UseVisualStyleBackColor = true;
            this.btnCipherFA.Click += new System.EventHandler(this.btnCipherFA_Click);
            // 
            // btnCipherEncrypt
            // 
            this.btnCipherEncrypt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCipherEncrypt.FlatAppearance.BorderSize = 0;
            this.btnCipherEncrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCipherEncrypt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCipherEncrypt.ForeColor = System.Drawing.Color.LightGray;
            this.btnCipherEncrypt.Location = new System.Drawing.Point(0, 0);
            this.btnCipherEncrypt.Name = "btnCipherEncrypt";
            this.btnCipherEncrypt.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnCipherEncrypt.Size = new System.Drawing.Size(250, 40);
            this.btnCipherEncrypt.TabIndex = 0;
            this.btnCipherEncrypt.Text = "Encrypt";
            this.btnCipherEncrypt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCipherEncrypt.UseVisualStyleBackColor = true;
            this.btnCipherEncrypt.Click += new System.EventHandler(this.btnCipherEncrypt_Click);
            // 
            // btnCipherMenu
            // 
            this.btnCipherMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCipherMenu.FlatAppearance.BorderSize = 0;
            this.btnCipherMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCipherMenu.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCipherMenu.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnCipherMenu.Image = global::WindowsFormsApp1.Properties.Resources.outline_lock_white_18dp;
            this.btnCipherMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCipherMenu.Location = new System.Drawing.Point(0, 413);
            this.btnCipherMenu.Name = "btnCipherMenu";
            this.btnCipherMenu.Size = new System.Drawing.Size(250, 45);
            this.btnCipherMenu.TabIndex = 6;
            this.btnCipherMenu.Text = "Cipher";
            this.btnCipherMenu.UseVisualStyleBackColor = true;
            this.btnCipherMenu.Click += new System.EventHandler(this.btnCipherMenu_Click);
            // 
            // panelKeySubMenu
            // 
            this.panelKeySubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(29)))));
            this.panelKeySubMenu.Controls.Add(this.btnKeyExport);
            this.panelKeySubMenu.Controls.Add(this.btnKeyImport);
            this.panelKeySubMenu.Controls.Add(this.btnKeyGenerate);
            this.panelKeySubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelKeySubMenu.Location = new System.Drawing.Point(0, 284);
            this.panelKeySubMenu.Name = "panelKeySubMenu";
            this.panelKeySubMenu.Size = new System.Drawing.Size(250, 129);
            this.panelKeySubMenu.TabIndex = 5;
            // 
            // btnKeyExport
            // 
            this.btnKeyExport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKeyExport.FlatAppearance.BorderSize = 0;
            this.btnKeyExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyExport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnKeyExport.ForeColor = System.Drawing.Color.LightGray;
            this.btnKeyExport.Location = new System.Drawing.Point(0, 80);
            this.btnKeyExport.Name = "btnKeyExport";
            this.btnKeyExport.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnKeyExport.Size = new System.Drawing.Size(250, 43);
            this.btnKeyExport.TabIndex = 2;
            this.btnKeyExport.Text = "Export";
            this.btnKeyExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeyExport.UseVisualStyleBackColor = true;
            this.btnKeyExport.Click += new System.EventHandler(this.btnKeyExport_Click);
            // 
            // btnKeyImport
            // 
            this.btnKeyImport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKeyImport.FlatAppearance.BorderSize = 0;
            this.btnKeyImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyImport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnKeyImport.ForeColor = System.Drawing.Color.LightGray;
            this.btnKeyImport.Location = new System.Drawing.Point(0, 40);
            this.btnKeyImport.Name = "btnKeyImport";
            this.btnKeyImport.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnKeyImport.Size = new System.Drawing.Size(250, 40);
            this.btnKeyImport.TabIndex = 1;
            this.btnKeyImport.Text = "Import";
            this.btnKeyImport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeyImport.UseVisualStyleBackColor = true;
            this.btnKeyImport.Click += new System.EventHandler(this.btnKeyImport_Click);
            // 
            // btnKeyGenerate
            // 
            this.btnKeyGenerate.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKeyGenerate.FlatAppearance.BorderSize = 0;
            this.btnKeyGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyGenerate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnKeyGenerate.ForeColor = System.Drawing.Color.LightGray;
            this.btnKeyGenerate.Location = new System.Drawing.Point(0, 0);
            this.btnKeyGenerate.Name = "btnKeyGenerate";
            this.btnKeyGenerate.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnKeyGenerate.Size = new System.Drawing.Size(250, 40);
            this.btnKeyGenerate.TabIndex = 0;
            this.btnKeyGenerate.Text = "Generate";
            this.btnKeyGenerate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeyGenerate.UseVisualStyleBackColor = true;
            this.btnKeyGenerate.Click += new System.EventHandler(this.btnKeyGenerate_Click);
            // 
            // btnKeyMenu
            // 
            this.btnKeyMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnKeyMenu.FlatAppearance.BorderSize = 0;
            this.btnKeyMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKeyMenu.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnKeyMenu.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnKeyMenu.Image = global::WindowsFormsApp1.Properties.Resources.outline_vpn_key_white_18dp;
            this.btnKeyMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKeyMenu.Location = new System.Drawing.Point(0, 239);
            this.btnKeyMenu.Name = "btnKeyMenu";
            this.btnKeyMenu.Size = new System.Drawing.Size(250, 45);
            this.btnKeyMenu.TabIndex = 4;
            this.btnKeyMenu.Text = "Key";
            this.btnKeyMenu.UseVisualStyleBackColor = true;
            this.btnKeyMenu.Click += new System.EventHandler(this.btnKeyMenu_Click);
            // 
            // panelFileSubMenu
            // 
            this.panelFileSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(29)))));
            this.panelFileSubMenu.Controls.Add(this.btnFileSaveAs);
            this.panelFileSubMenu.Controls.Add(this.btnFileSave);
            this.panelFileSubMenu.Controls.Add(this.btnFileOpen);
            this.panelFileSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFileSubMenu.Location = new System.Drawing.Point(0, 110);
            this.panelFileSubMenu.Name = "panelFileSubMenu";
            this.panelFileSubMenu.Size = new System.Drawing.Size(250, 129);
            this.panelFileSubMenu.TabIndex = 3;
            // 
            // btnFileSaveAs
            // 
            this.btnFileSaveAs.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFileSaveAs.FlatAppearance.BorderSize = 0;
            this.btnFileSaveAs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFileSaveAs.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFileSaveAs.ForeColor = System.Drawing.Color.LightGray;
            this.btnFileSaveAs.Location = new System.Drawing.Point(0, 80);
            this.btnFileSaveAs.Name = "btnFileSaveAs";
            this.btnFileSaveAs.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnFileSaveAs.Size = new System.Drawing.Size(250, 40);
            this.btnFileSaveAs.TabIndex = 2;
            this.btnFileSaveAs.Text = "Save as...";
            this.btnFileSaveAs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFileSaveAs.UseVisualStyleBackColor = true;
            this.btnFileSaveAs.Click += new System.EventHandler(this.btnFileSaveAs_Click);
            // 
            // btnFileSave
            // 
            this.btnFileSave.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFileSave.FlatAppearance.BorderSize = 0;
            this.btnFileSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFileSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFileSave.ForeColor = System.Drawing.Color.LightGray;
            this.btnFileSave.Location = new System.Drawing.Point(0, 40);
            this.btnFileSave.Name = "btnFileSave";
            this.btnFileSave.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnFileSave.Size = new System.Drawing.Size(250, 40);
            this.btnFileSave.TabIndex = 1;
            this.btnFileSave.Text = "Save";
            this.btnFileSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFileSave.UseVisualStyleBackColor = true;
            this.btnFileSave.Click += new System.EventHandler(this.btnFileSave_Click);
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFileOpen.FlatAppearance.BorderSize = 0;
            this.btnFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFileOpen.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFileOpen.ForeColor = System.Drawing.Color.LightGray;
            this.btnFileOpen.Location = new System.Drawing.Point(0, 0);
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnFileOpen.Size = new System.Drawing.Size(250, 40);
            this.btnFileOpen.TabIndex = 0;
            this.btnFileOpen.Text = "Open";
            this.btnFileOpen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFileOpen.UseVisualStyleBackColor = true;
            this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
            // 
            // btnFile
            // 
            this.btnFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFile.FlatAppearance.BorderSize = 0;
            this.btnFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFile.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnFile.ForeColor = System.Drawing.Color.Gainsboro;
            this.btnFile.Image = global::WindowsFormsApp1.Properties.Resources.outline_description_white_18dp;
            this.btnFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFile.Location = new System.Drawing.Point(0, 65);
            this.btnFile.Name = "btnFile";
            this.btnFile.Size = new System.Drawing.Size(250, 45);
            this.btnFile.TabIndex = 2;
            this.btnFile.Text = "File";
            this.btnFile.UseVisualStyleBackColor = true;
            this.btnFile.Click += new System.EventHandler(this.btnFile_Click);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 65);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(250, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(737, 65);
            this.panel2.TabIndex = 1;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Mongolian Baiti", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(704, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(30, 30);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "♥\r\n";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(250, 65);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(737, 629);
            this.panelChildForm.TabIndex = 2;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(987, 694);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelSideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(987, 694);
            this.Name = "Home";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelSideMenu.ResumeLayout(false);
            this.panelCipherSubMenu.ResumeLayout(false);
            this.panelKeySubMenu.ResumeLayout(false);
            this.panelFileSubMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Panel panelFileSubMenu;
        private System.Windows.Forms.Button btnFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFileSaveAs;
        private System.Windows.Forms.Button btnFileSave;
        private System.Windows.Forms.Button btnFileOpen;
        private System.Windows.Forms.Panel panelKeySubMenu;
        private System.Windows.Forms.Button btnKeyExport;
        private System.Windows.Forms.Button btnKeyImport;
        private System.Windows.Forms.Button btnKeyGenerate;
        private System.Windows.Forms.Button btnKeyMenu;
        private System.Windows.Forms.Panel panelCipherSubMenu;
        private System.Windows.Forms.Button btnCipherFA;
        private System.Windows.Forms.Button btnCipherEncrypt;
        private System.Windows.Forms.Button btnCipherMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnCipherDecrypt;
    }
}

